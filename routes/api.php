<?php

use App\Http\Controllers\CategoryController;
use App\Http\Controllers\UserController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});
// Route::resource('category',CategoryController::class)->only(['index','create','show','update','destroy']);
Route::post('category/add',[CategoryController::class,'create']);
Route::post('category/update',[CategoryController::class,'update']);
Route::get('category/list',[CategoryController::class,'index']);
Route::get('category/{id}',[CategoryController::class,'show']);
Route::post('category/{id}',[CategoryController::class,'update']);
Route::delete('category/{category}',[CategoryController::class,'destroy']);
Route::post('user/add',[UserController::class,'create']);
Route::get('user/list',[UserController::class,'index']);
Route::get('user/{id}',[UserController::class,'show']);
Route::post('user/{id}',[UserController::class,'update']);
Route::delete('user/{id}',[UserController::class,'destroy']);

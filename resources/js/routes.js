const Welcome = () => import('./components/Welcome.vue' /* webpackChunkName: "resource/js/components/welcome" */)
const CategoryList = () => import('./components/category/List.vue' /* webpackChunkName: "resource/js/components/category/list" */)
const CategoryCreate = () => import('./components/category/Add.vue' /* webpackChunkName: "resource/js/components/category/add" */)
const CategoryEdit = () => import('./components/category/Edit.vue' /* webpackChunkName: "resource/js/components/category/edit" */)
const UserList= ()=> import('./components/Users/List.vue')
const AddUser= ()=> import('./components/Users/Add.vue')
const EditUser= ()=> import('./components/Users/Edit.vue')

export const routes = [
    {
        name: 'home',
        path: '/',
        component: Welcome
    },
    {
        name: 'categoryList',
        path: '/category',
        component: CategoryList
    },
    {
        name: 'categoryEdit',
        path: '/category/:id/edit',
        component: CategoryEdit
    },
    {
        name: 'categoryAdd',
        path: '/category/add',
        component: CategoryCreate
    },
    {
        name:'userList',
        path:'/user',
        component: UserList
    },
    {
        name:'AddUser',
        path:'/users/add',
        component:AddUser
    },
    {
        name:'EditUser',
        path:'/user/:id/edit',
        component:EditUser
    }
]
